package easydht

import (
	"encoding/hex"
	"fmt"
)

// XOR returns exclusive disjunction of two keys
func XOR(key1, key2 string) string {
	c := make([]byte, len(key1))
	for i := 0; i < len(key1); i++ {
		c[i] = key1[i] ^ key2[i]
	}
	return string(c)
}

// LeadingZeroBitsLen returns the number of leading zero bits
func LeadingZeroBitsLen(id string) int {
	for i := 0; i < len(id); i++ {
		for j := 0; j < 8; j++ {
			if (id[i]>>uint8(7-j))&0x1 != 0 {
				return i*8 + j
			}
		}
	}
	return len(id) * 8
}

func decodeID(encodedID string, requiredLength int) (string, error) {
	decodedID, err := hex.DecodeString(encodedID)
	if err != nil {
		return "", fmt.Errorf("error when decode ID, %s", err)
	}
	id := string(decodedID)
	if id == "" {
		return "", fmt.Errorf("id must be provided")
	}
	if len(id) != requiredLength / 8 {
		return "", fmt.Errorf("id must contain %d bytes", requiredLength)
	}
	return id, nil
}

func encodeID(id string) string {
	return hex.EncodeToString([]byte(id))
}