package easydht

import (
	"log"
	"net"
	"strconv"
	"time"
)

type Config struct {
	NodeID            string
	Address           string
	BootstrapNodes    []string
	ParallelismDegree int // degree of parallelism in network calls
	KeySize           int // size in bits of the keys
}

func NewDefaultConfig() *Config {
	return &Config{
		NodeID:            "",
		Address:           "127.0.0.1:8787",
		BootstrapNodes:    []string{},
		ParallelismDegree: 3,
		KeySize:           160,
	}
}

type DHT struct {
	routingTable *RoutingTable
	config       *Config
	rpcHandler   *RPCHandler
}

func (d *DHT) Contacts() []*Contact {
	result := []*Contact{}
	for _, bucket := range d.routingTable.nodes {
		for current := bucket.Front(); current != nil; current = current.Next() {
			result = append(result, current.Value.(*Contact))
		}
	}

	return result
}

func NewDHT(config *Config) *DHT {
	nodeID := config.NodeID
	if nodeID == "" {
		nodeID = NewRandomNodeID(config.KeySize)
	}
	host, port, err := net.SplitHostPort(config.Address)
	if err != nil {
		log.Fatalf("error when parse local node address: %s", err)
	}
	p, err := strconv.Atoi(port)
	if err != nil {
		log.Fatal("error when parse local node address: not valid port")
	}
	contact := Contact{
		ID:   nodeID,
		Host: host,
		Port: p,
	}
	return &DHT{
		routingTable: NewRoutingTable(&contact, 20, config.KeySize),
		config:       config,
	}
}

func (d *DHT) Start() {
	handler, err := NewRPCHandler(d.routingTable)
	if err != nil {
		log.Fatal(err)
	}
	d.rpcHandler = handler
	d.bootstrap()
	_, err = d.rpcHandler.IterativeFindNode(d.routingTable.self.ID)
	if err != nil {
		log.Fatal(err)
	}
}

func (d *DHT) bootstrap() {
	done := make(chan *remoteNodeResponse)
	for _, address := range d.config.BootstrapNodes {
		d.rpcHandler.Ping(address, &done)
	}

	select {
	case _ = <-done:
		return
	case <-time.After(time.Millisecond * 1000):
		log.Fatalf("[%s] unable to join the network: nodes are not responding", d.routingTable.self.Address())
	}
}
