package easydht

import (
	"container/list"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"sync"
)

type Contact struct {
	ID   string
	Host string
	Port int
}

type sortableContacts []*sortableContact

func isContainContact(contacts []*sortableContact, contact *sortableContact) bool {
	for _, c := range contacts {
		if c.Contact.ID == contact.Contact.ID {
			return true
		}
	}

	return false
}

func addContact(contacts []*sortableContact, contact *sortableContact) []*sortableContact {
	if isContainContact(contacts, contact) {
		return contacts
	}
	if len(contacts) == 0 {
		return append(contacts, contact)
	}
	for i := 0; i < len(contacts); i++ {
		if contact.Less(contacts[i]) {
			contacts = append(contacts, contacts[i])
			contacts[i] = contact
			break
		} else {
			if i == len(contacts) - 1 {
				contacts = append(contacts, contact)
				break
			}
		}
	}
	return contacts
}

type sortableContact struct {
	Distance string
	Contact  *Contact
}

func (contact1 *sortableContact) Less(contact2 *sortableContact) bool {
	k1 := contact1.Distance
	k2 := contact2.Distance
	for i := 0; i < len(k1); i++ {
		if k1[i] != k2[i] {
			return k1[i] < k2[i]
		}
	}
	return false
}

func (c *Contact) IDHex() string {
	return hex.EncodeToString([]byte(c.ID))
}

func (c *Contact) Address() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

type RoutingTable struct {
	self       *Contact
	nodes      []*list.List
	peers      []*list.List
	bucketSize int
	keyLength  int
	m          sync.RWMutex
}

func newSortableContact(contact *Contact, targetID string) *sortableContact {
	return &sortableContact{XOR(contact.ID, targetID), contact}
}

func copyContacts(front *list.Element, contacts []*sortableContact, targetID string) []*sortableContact {
	for currentElem := front; currentElem != nil; currentElem = currentElem.Next() {
		contact := currentElem.Value.(*Contact)
		contacts = addContact(contacts, newSortableContact(contact, targetID))
	}
	return contacts
}

func (rt *RoutingTable) FindClosest(id string, count int) []*sortableContact {
	bucketNumber := LeadingZeroBitsLen(XOR(rt.self.ID, id))
	if bucketNumber == rt.keyLength {
		bucketNumber = bucketNumber - 1
	}
	sortableContacts := []*sortableContact{}
	sortableContacts = copyContacts(rt.nodes[bucketNumber].Front(), sortableContacts, id)
	for i := 0; i < rt.keyLength; i++ {
		if bucketNumber - i >= 0 {
			sortableContacts = copyContacts(rt.nodes[bucketNumber - i].Front(), sortableContacts, id)
		}
		if bucketNumber + i < rt.keyLength {
			sortableContacts = copyContacts(rt.nodes[bucketNumber + i].Front(), sortableContacts, id)
		}
		if len(sortableContacts) >= count {
			break
		}
	}
	return sortableContacts
}

func (rt *RoutingTable) Update(contact *Contact) {
	prefixLen := LeadingZeroBitsLen(XOR(rt.self.ID, contact.ID))
	if prefixLen == rt.keyLength {
		return
	}
	bucket := rt.nodes[prefixLen]
	rt.m.Lock()
	defer rt.m.Unlock()
	element := bucket.Front()
	for {
		if element == nil {
			break
		}
		c := element.Value.(*Contact)
		if c.ID == contact.ID {
			bucket.MoveToFront(element)
			return
		}
		element = element.Next()
	}
	bucket.PushFront(contact)
}

func NewRoutingTable(contact *Contact, bucketSize, keyLength int) *RoutingTable {
	if bucketSize == 0 {
		bucketSize = 20
	}
	buckets := []*list.List{}
	for i := 0; i < keyLength; i++ {
		buckets = append(buckets, list.New())
	}
	return &RoutingTable{
		self:       contact,
		nodes:      buckets,
		peers:      buckets,
		bucketSize: bucketSize,
		keyLength:  keyLength,
	}
}

func NewRandomNodeID(idLength int) string {
	id := make([]byte, idLength / 8)
	rand.Read(id)
	return string(id)
}
