package easydht

import (
	"encoding/binary"
	"fmt"
	"testing"
	"time"
	"strings"
)

type nodeInfo struct {
	id      string
	address string
}

func startDHT(c *Contact, bootstrapNodes []string) *DHT {
	config := NewDefaultConfig()
	config.KeySize = 32
	config.Address = c.Address()
	config.NodeID = c.ID
	config.BootstrapNodes = bootstrapNodes
	d := NewDHT(config)
	go d.Start()

	return d
}

func TestRPCClient_Lookup(t *testing.T) {
	contacts := []*sortableContact{}
	buffer := make([]byte, 4)
	binary.BigEndian.PutUint32(buffer, 0)
	requestingNodeID := string(buffer)
	requestingContact := Contact{requestingNodeID, "127.0.0.1", 7099}
	var i uint32 = 0
	for i = 1; i <= 30; i++ {
		binary.BigEndian.PutUint32(buffer, i)
		c := Contact{
			string(buffer),
			"127.0.0.1",
			7100 + int(i),
		}
		contacts = addContact(contacts, newSortableContact(&c, requestingNodeID))
	}

	bootstrapNodes1 := []string{contacts[29].Contact.Address()}
	bootstrapNodes2 := []string{contacts[14].Contact.Address()}

	bootstrapNode1 := startDHT(contacts[29].Contact, bootstrapNodes1)
	time.Sleep(time.Millisecond * 100)

	bootstrapNode2 := startDHT(contacts[14].Contact, bootstrapNodes1)
	time.Sleep(time.Millisecond * 100)

	fmt.Printf("BN 1 [%s %s]: %s\n", bootstrapNode1.routingTable.self.Address(), bootstrapNode1.routingTable.self.IDHex(), bootstrapNode1.Contacts()[0].IDHex())
	fmt.Printf("BN 2 [%s %s]: %s\n", bootstrapNode2.routingTable.self.Address(), bootstrapNode2.routingTable.self.IDHex(), bootstrapNode2.Contacts()[0].IDHex())
	// join to bootstrap node 2
	for _, c := range contacts[0:14] {
		startDHT(c.Contact, bootstrapNodes2)
	}

	time.Sleep(time.Millisecond * 100)
	cs1 := []string{}
	for _, cc := range bootstrapNode2.Contacts() {
		cs1 = append(cs1, cc.IDHex())
	}
	fmt.Printf("BN 2 [%s %s]: %s\n", bootstrapNode2.routingTable.self.Address(), bootstrapNode2.routingTable.self.IDHex(), strings.Join(cs1[:],","))
	// join to bootstrap node 1
	for _, c := range contacts[len(contacts)-15 : len(contacts)-1] {
		startDHT(c.Contact, bootstrapNodes1)
	}
	time.Sleep(time.Millisecond * 100)
	cs2 := []string{}
	for _, cc := range bootstrapNode1.Contacts() {
		cs2 = append(cs2, cc.IDHex())
	}
	fmt.Printf("BN 1 [%s %s]: %s\n", bootstrapNode1.routingTable.self.Address(), bootstrapNode1.routingTable.self.IDHex(), strings.Join(cs2[:],","))
	fmt.Println("Requesting nodeID: ", requestingContact.IDHex())

	requestingNode := startDHT(&requestingContact, bootstrapNodes2)

	time.Sleep(time.Millisecond * 1000)

	result, err := requestingNode.rpcHandler.IterativeFindNode(contacts[1].Contact.ID)
	if err != nil {
		t.Errorf("error when iterative find node: %s", err)
	}
	for _, r := range result {
		fmt.Printf("%d, %x, %b, %d\n", []byte(r.Contact.ID), requestingContact.ID, []byte(XOR(r.Contact.ID, requestingContact.ID)), LeadingZeroBitsLen(XOR(r.Contact.ID, requestingContact.ID)))
	}
}
