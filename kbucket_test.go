package easydht

import (
	"testing"
)

var defaultContactId = []byte{2, 9, 4, 2, 9, 4, 5, 5} // 00000010 00001001 00000100 00000010 00001001 00000100 00000101 00000101

func newContact(id []byte) *Contact {
	return &Contact{
		ID:   string(id),
		Host: "127.0.0.1",
		Port: 6666,
	}
}

func TestRoutingTable_Update(t *testing.T) {
	rt := NewRoutingTable(newContact(defaultContactId), 20, 64)
	cases := []struct {
		in                   *Contact
		expectedBucketNumber int
		expectedBucketLen    int
	}{
		{
			newContact([]byte{9, 4, 4, 2, 9, 4, 5, 5}), // 00001001 00000100 00000100 00000010 00001001 00000100 00000101 00000101
			4,
			1,
		},
		// insert already existed contact
		{
			newContact([]byte{9, 4, 4, 2, 9, 4, 5, 5}), // 00001001 00000100 00000100 00000010 00001001 00000100 00000101 00000101
			4,
			1,
		},
		// insert new contact in the same bucket
		{
			newContact([]byte{9, 5, 4, 2, 9, 4, 5, 5}), // 00001001 00000110 00000100 00000010 00001001 00000100 00000101 00000101
			4,
			2,
		},
	}
	for _, tt := range cases {
		rt.Update(tt.in)
		if rt.nodes[tt.expectedBucketNumber].Len() != tt.expectedBucketLen {
			t.Errorf("bucket %d should contain %d contacts, but its contain %d",
				tt.expectedBucketNumber,
				tt.expectedBucketLen,
				rt.nodes[tt.expectedBucketNumber].Len(),
			)
		}
	}
}

func TestRoutingTable_FindClosest(t *testing.T) {
	rt := NewRoutingTable(newContact(defaultContactId), 20, 64)
	contactsToAdd := []*Contact{
		newContact([]byte{9, 4, 4, 2, 9, 4, 5, 5}),   // 00001001 00000100 00000100 00000010 00001001 00000100 00000101 00000101
		newContact([]byte{255, 4, 4, 2, 9, 4, 5, 5}), // 11111111 00000100 00000100 00000010 00001001 00000100 00000101 00000101
		newContact([]byte{4, 255, 4, 2, 9, 4, 5, 5}), // 00000100 11111111 00000100 00000010 00001001 00000100 00000101 00000101
		newContact([]byte{100, 4, 4, 2, 9, 4, 5, 5}), // 01100100 00000100 00000100 00000010 00001001 00000100 00000101 00000101
		newContact([]byte{9, 100, 4, 2, 9, 4, 5, 5}), // 00001001 01100100 00000100 00000010 00001001 00000100 00000101 00000101
	}
	for _, contactToAdd := range contactsToAdd {
		rt.Update(contactToAdd)
	}
	actualContacts := rt.FindClosest(string(defaultContactId), 4)
	if len(actualContacts) != 4 {
		t.Errorf("expected 4 closest contacts, given %d", len(actualContacts))
	}
	closestContacts := [][]byte{
		[]byte{4, 255, 4, 2, 9, 4, 5, 5}, // Prefix length: 5
		[]byte{9, 4, 4, 2, 9, 4, 5, 5},   // Prefix length: 4
		[]byte{9, 100, 4, 2, 9, 4, 5, 5}, // Prefix length: 4
		[]byte{100, 4, 4, 2, 9, 4, 5, 5}, // Prefix length: 1
	}
	for i, actualContact := range actualContacts {
		if string(closestContacts[i]) != actualContact.Contact.ID {
			t.Errorf("wrong contacts order")
		}
	}
}
